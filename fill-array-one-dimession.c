#include <stdio.h>
int main ()
{
  int n, i, array_nums[10], k;

  // Prompt user for input
  printf("Input an integer (2-10)\n");
  scanf("%d", &n);

  if (n >= 2 && n <= 10)
  {
    k = 0;

    // Loop to fill array with sequential numbers, starting from 0
    for (i = 0; i < 10; i++)
    {
      array_nums[i] = k;
      k++;

      if (k == n)
      {
        k = 0;
      }
    }

    // Loop to print array elements
    for (i = 0; i < 10; i++)
    {
      printf("array_nums[%d] = %d\n", i, array_nums[i]);
    }
  }
  return 0; // End of program
}